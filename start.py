import app
import time
import os
import sys
import argparse

def main():
        parser = argparse.ArgumentParser(description="Reddit Updater on Telegram")
        parser.add_argument('-r', '--run', help='run the program', action='store_true')
        parser.add_argument('-l','--link', type=str, help='link to run')
        parser.add_argument('-c', '--configure', help='telegram configuration', action='store_true')
        args = parser.parse_args()
        if args.link != None:
                FirstLoad(args.link)
                Load(args.link)    
        elif args.configure:
                ConfigureTelegram()
def ConfigureTelegram():
        app.ConfigureTelegram()
def FirstLoad(link):
    app.CheckTelegramConf()
    request = app.RequestPage(link)
    if request.status_code == 200:
        app.WriteJsonToFile(request)
def Load(link):
        while True:
                app.Main(link)
                time.sleep(20)

#Program Start
if __name__ == "__main__":
        main()