import requests
import json
import time
import telegram_send
import os

def WriteJsonToFile(req):
    with open("elements.json", 'w') as json_file:
        json_file.write(req.text)

def RequestPage(_link):
    link = "{}.json".format(_link)
    req = requests.get(link, headers={'User-agent': 'MatBot'})
    if(req.status_code == 200):
        return req
def OpenJson():
    with open('elements.json', 'r') as json_file:
        _elements = json.load(json_file)
    return _elements

def ConfigureTelegram(path = 'telegram_configuration.conf'):
    telegram_send.configure(path)

def CheckTelegramConf(path = 'telegram_configuration.conf'):
    if not os.path.exists(path):
        telegram_send.configure(path)

def Main(link):
    req = RequestPage(link)
    if req.status_code == 200:
        if not os.path.exists('elements.json'):  # if file does not exists
            WriteJsonToFile(r)  # Write to file
        elements = OpenJson()
        elements_new = json.loads(req.text)
        # Get element from current request
        newEl = elements_new["data"]["children"][0]['data']['author']
        # Get element from json file
        oldEl = elements["data"]["children"][0]['data']['author']
        print('newEl: ' + newEl)
        print('oldEl: ' + oldEl)
        isNew = True
        if newEl != oldEl:  # If authors are not equals
            i = 0
            # Print and send new elements
            author_fullname = elements_new["data"]["children"][i]['data']['author']
            isNew = elements_new["data"]["children"][i]['data']['author'] != elements["data"]["children"][0]['data']['author']
            if isNew:
                title = elements_new["data"]["children"][i]["data"]["title"]
                subreddit = elements_new["data"]["children"][i]["data"]["subreddit_name_prefixed"]
                image_url = elements_new["data"]["children"][i]["data"]["url"]
                print(title)
                to_Telegram = "Title: {}\nAuthor: {}\nSubreddit: {}\nImage Url: {}".format(
                    title, author_fullname, subreddit, image_url)
                telegram_send.send(
                    messages=[to_Telegram], conf='telegram_configuration.conf')
        WriteJsonToFile(req)  # Write to Json file current request


#        try:
#                opts, args = getopt.getopt(args, 'hcrl:', ["help", "configure", "run", "link="])
#        except getopt.GetoptError as err:
#                print(str(err))  
#                sys.exit(2)
#        for opt, arg in opts:
#                if(opt in ('-h', '--help')):
#                        print('usage: 1 arguments needed')
#                        sys.exit()
#                elif opt in ('-c', '--configure'):
#                        app.ConfigureTelegram()
#                elif opt in ('-r', '--run'):
#                        FirstLoad()
#                        Load()
#                elif opt in ('-l', 'link'):       