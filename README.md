# Reddit Updater

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Reddit updater is a simple python script that send you, using Telegram new post or messages from reddit.  

  - Simple and fast
  - Written in Python 3
  - Simple Configuration
 ---
### Getting Started
```console
$ python 3.x start.py -l https://www.reddit.com/r/SubReddit/new/
```
